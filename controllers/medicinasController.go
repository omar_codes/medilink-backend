package controllers

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/omar_codes/medilink/models"
	"gitlab.com/omar_codes/medilink/utils"
)

// AddMedicinas agrega las medicinas de una consulta que no esten registradas
func AddMedicinas(tratamientos []models.Tratamiento) {
	db := utils.GetConnection()
	defer db.Close()
	for _, item := range tratamientos {
		medicina := models.Medicina{}
		nombreMedicina := strings.Title(item.Medicina)
		db.Where("nombre = ?", nombreMedicina).First(&medicina)
		if medicina.ID == 0 {
			newMedicina := models.Medicina{Nombre: nombreMedicina}
			db.Create(&newMedicina)
		}
	}
}

// GetSuggestions obtiene la sugerencia de nombre de medicinas
func GetSuggestions(w http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()
	keyword := queryParams.Get("keyword")
	medicinas := []models.Medicina{}
	db := utils.GetConnection()
	defer db.Close()
	db.Where("nombre LIKE ?", "%"+keyword+"%").Order("nombre ASC").Find(&medicinas)
	data := []string{}
	for _, item := range medicinas {
		data = append(data, item.Nombre)
	}
	j, _ := json.Marshal(data)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetMedicinas obtiene las medicinas registradas
func GetMedicinas(w http.ResponseWriter, r *http.Request) {
	medicinas := []models.Medicina{}
	db := utils.GetConnection()
	defer db.Close()
	db.Order("nombre ASC").Find(&medicinas)
	j, _ := json.Marshal(medicinas)
	utils.SendResponse(w, http.StatusOK, j)
}

// StoreMedicina guarda un registro de medicina
func StoreMedicina(w http.ResponseWriter, r *http.Request) {
	medicina := models.Medicina{}
	db := utils.GetConnection()
	defer db.Close()
	err := json.NewDecoder(r.Body).Decode(&medicina)
	if err != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}
	db.Create(&medicina)
	j, _ := json.Marshal(medicina)
	utils.SendResponse(w, http.StatusCreated, j)
}

// UpdateMedicina modifica un registro de medicina
func UpdateMedicina(w http.ResponseWriter, r *http.Request) {
	medicinaFind := models.Medicina{}
	medicinaData := models.Medicina{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&medicinaFind, id)
	if medicinaFind.ID > 0 {
		err := json.NewDecoder(r.Body).Decode(&medicinaData)
		if err != nil {
			utils.SendErr(w, http.StatusBadRequest)
			return
		}
		db.Model(&medicinaFind).Update(medicinaData)
		j, _ := json.Marshal(medicinaFind)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// DeleteMedicina borra un registro de medicina
func DeleteMedicina(w http.ResponseWriter, r *http.Request) {
	medicina := models.Medicina{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&medicina, id)
	if medicina.ID > 0 {
		db.Delete(medicina)
		utils.SendResponse(w, http.StatusOK, []byte(`{}`))
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}
