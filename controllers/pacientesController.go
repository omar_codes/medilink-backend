package controllers

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/models"
	"gitlab.com/omar_codes/medilink/utils"
)

// GetPacientes obtiene los pacientes
func GetPacientes(w http.ResponseWriter, r *http.Request) {
	pacientes := []models.Paciente{}
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&pacientes)
	for i := 0; i < len(pacientes); i++ {
		pacientes[i].Edad = utils.Age(*pacientes[i].Nacimiento)
	}
	j, _ := json.Marshal(pacientes)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetPaciente obtiene un paciente y sus consultas
func GetPaciente(w http.ResponseWriter, r *http.Request) {
	paciente := models.Paciente{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	// db.Preload("Consultas", func(db *gorm.DB) *gorm.DB {
	// 	return db.Order("fecha DESC")
	// }).Find(&paciente, id)
	db.Find(&paciente, id)
	if paciente.ID > 0 {
		var edad int
		if paciente.Nacimiento.Year() != 1 {
			edad = utils.Age(*paciente.Nacimiento)
		} else {
			edad = -1
		}
		paciente.Edad = edad
		j, _ := json.Marshal(paciente)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// SearchPaciente busca un paciente por nombre
func SearchPaciente(w http.ResponseWriter, r *http.Request) {
	queryParam := r.URL.Query()
	keyword := queryParam.Get("keyword")
	pacientes := []models.Paciente{}
	db := utils.GetConnection()
	defer db.Close()
	db.Where("nombre LIKE ?", "%"+keyword+"%").Order("nombre ASC").Find(&pacientes)
	j, _ := json.Marshal(pacientes)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetConsultasByPaciente obtiene todas las consultas de un paciente
func GetConsultasByPaciente(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	consultas := []models.Consulta{}
	db := utils.GetConnection()
	defer db.Close()
	db.Preload("Tratamientos").Where("paciente_id = ?", id).Order("fecha DESC").Find(&consultas)
	j, _ := json.Marshal(consultas)
	utils.SendResponse(w, http.StatusOK, j)
}

// StorePaciente guarda un registro de paciente
func StorePaciente(w http.ResponseWriter, r *http.Request) {
	paciente := models.Paciente{}
	db := utils.GetConnection()
	defer db.Close()
	err := json.NewDecoder(r.Body).Decode(&paciente)
	if err != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}
	date, _ := time.Parse("2006-01-02", paciente.FechaString)
	paciente.Nacimiento = &date
	err = db.Create(&paciente).Error
	if err != nil {
		utils.SendErr(w, http.StatusInternalServerError)
		return
	}
	j, _ := json.Marshal(paciente)
	utils.SendResponse(w, http.StatusCreated, j)
}

// UpdatePaciente modifica un registro de paciente
func UpdatePaciente(w http.ResponseWriter, r *http.Request) {
	pacienteFound := models.Paciente{}
	pacienteData := models.Paciente{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&pacienteFound, id)
	if pacienteFound.ID > 0 {
		err := json.NewDecoder(r.Body).Decode(&pacienteData)
		if err != nil {
			utils.SendErr(w, http.StatusBadRequest)
			return
		}
		date, _ := time.Parse("2006-01-02", pacienteData.FechaString)
		pacienteData.Nacimiento = &date
		db.Model(&pacienteFound).Updates(pacienteData)
		j, _ := json.Marshal(pacienteFound)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// DeletePaciente borra un registro de paciente
func DeletePaciente(w http.ResponseWriter, r *http.Request) {
	paciente := models.Paciente{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&paciente, id)
	if paciente.ID > 0 {
		db.Delete(paciente)
		utils.SendResponse(w, http.StatusOK, []byte(`{}`))
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}
