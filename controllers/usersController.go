package controllers

import (
	"encoding/json"
	"net/http"
	"gitlab.com/omar_codes/medilink/models"
	"gitlab.com/omar_codes/medilink/utils"
)

// Login inicio de sesion
func Login(w http.ResponseWriter, r *http.Request)  {
	userFind := models.User{}
	userData := models.User{}
	db := utils.GetConnection()
	defer db.Close()
	err := json.NewDecoder(r.Body).Decode(&userData)
	if err != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}
	db.Where("username = ?", userData.Username).First(&userFind)
	if userFind.ID > 0 {
		if utils.CheckPassword(userData.Password, userFind.Password) {
			userFind.Password = ""
			j, _ := json.Marshal(userFind)
			utils.SendResponse(w, http.StatusOK, j)
		} else {
			utils.SendErr(w, http.StatusUnauthorized)
		}
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}
