package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	"gitlab.com/omar_codes/medilink/models"
	"gitlab.com/omar_codes/medilink/utils"
)

// GetConsulta obtiene un registro de consulta
func GetConsulta(w http.ResponseWriter, r *http.Request) {
	consulta := models.Consulta{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&consulta, id)
	if consulta.ID > 0 {
		utils.SendErr(w, http.StatusNotFound)
		return
	}
	j, _ := json.Marshal(consulta)
	utils.SendResponse(w, http.StatusOK, j)
}

// StoreConsulta guarda un registro de consulta
func StoreConsulta(w http.ResponseWriter, r *http.Request) {
	consulta := models.Consulta{}
	db := utils.GetConnection()
	defer db.Close()
	err := json.NewDecoder(r.Body).Decode(&consulta)
	if err != nil {
		fmt.Println(err)
		utils.SendErr(w, http.StatusBadRequest)
		return
	}
	date, _ := time.Parse("2006-01-02", consulta.FechaString)
	consulta.Fecha = &date
	err = db.Create(&consulta).Error
	if err != nil {
		utils.SendErr(w, http.StatusInternalServerError)
		return
	}
	insertMedicinas(consulta.Tratamientos, db)
	j, _ := json.Marshal(consulta)
	utils.SendResponse(w, http.StatusCreated, j)
}

// inserta los nombres de las medicinas para sugerencias
func insertMedicinas(items []models.Tratamiento, db *gorm.DB) {
	for _, item := range items {
		medicina := models.Medicina{}
		db.Where("nombre = ?", item.Medicina).First(&medicina)
		if medicina.ID == 0 {
			newMedicina := models.Medicina{Nombre: strings.Title(item.Medicina)}
			db.Create(&newMedicina)
		}
	}
}

// UpdateConsulta modifica los datos de una consulta
func UpdateConsulta(w http.ResponseWriter, r *http.Request) {
	consultaFind := models.Consulta{}
	consultaData := models.Consulta{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&consultaFind, id)
	if consultaFind.ID > 0 {
		err := json.NewDecoder(r.Body).Decode(&consultaData)
		if err != nil {
			utils.SendErr(w, http.StatusBadRequest)
			return
		}
		date, _ := time.Parse("2006-01-02", consultaData.FechaString)
		consultaData.Fecha = &date
		db.Model(&consultaFind).Update(consultaData)
		j, _ := json.Marshal(consultaFind)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// DeleteConsulta borra un registro de consulta
func DeleteConsulta(w http.ResponseWriter, r *http.Request) {
	consulta := models.Consulta{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&consulta, id)
	if consulta.ID > 0 {
		db.Delete(consulta)
		utils.SendResponse(w, http.StatusOK, []byte(`{}`))
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// DeleteTratamiento borra un registro de tratamiento
func DeleteTratamiento(w http.ResponseWriter, r *http.Request) {
	tratamiento := models.Tratamiento{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()
	db.Find(&tratamiento, id)
	if tratamiento.ID > 0 {
		db.Delete(tratamiento)
		utils.SendResponse(w, http.StatusOK, []byte(`{}`))
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}
