package controllers

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/models"
	"gitlab.com/omar_codes/medilink/utils"
)

// GetCita obtiene un registro de citas por ID
func GetCita(w http.ResponseWriter, r *http.Request) {
	cita := models.Cita{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()

	db.Find(&cita, id)
	if cita.ID > 0 {
		j, _ := json.Marshal(cita)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// GetCitasByPacienteID obtiene las citas de un paciente por su ID
func GetCitasByPacienteID(w http.ResponseWriter, r *http.Request) {
	citas := []models.Cita{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()

	db.Where("pacienteID = ?", id).Find(&citas)
	j, _ := json.Marshal(citas)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetCitasByFecha obtiene las citas de una fecha
func GetCitasByFecha(w http.ResponseWriter, r *http.Request) {
	citas := []models.Cita{}
	fechaTmp := r.URL.Query().Get("fecha")
	fecha, err := time.Parse("2006-01-02", fechaTmp)
	db := utils.GetConnection()
	defer db.Close()
	if err != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}

	db.Where("fecha = ", fecha).Find(&citas)
	j, _ := json.Marshal(citas)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetCitasBetweenFechas obtiene las citas entre 2 fechas
func GetCitasBetweenFechas(w http.ResponseWriter, r *http.Request) {
	citas := []models.Cita{}
	iniTmp := r.URL.Query().Get("inicio")
	finTmp := r.URL.Query().Get("fin")
	ini, err1 := time.Parse("2006-01-02", iniTmp)
	fin, err2 := time.Parse("2006-01-02", finTmp)
	db := utils.GetConnection()
	defer db.Close()
	if err1 != nil || err2 != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}

	db.Where("fecha BETWEEN ? AND ?", ini, fin).Find(&citas)
	j, _ := json.Marshal(citas)
	utils.SendResponse(w, http.StatusOK, j)
}

// GetCitasToday obtiene las citas del dia
func GetCitasToday(w http.ResponseWriter, r *http.Request) {
	citas := []models.Cita{}
	today := time.Now().Format("2006-01-02")
	db := utils.GetConnection()
	defer db.Close()
	db.Where("fecha = ?", today).Find(&citas)
	j, _ := json.Marshal(citas)
	utils.SendResponse(w, http.StatusOK, j)
}

// StoreCita guarda un nueva cita
func StoreCita(w http.ResponseWriter, r *http.Request) {
	cita := models.Cita{}
	db := utils.GetConnection()
	defer db.Close()

	err := json.NewDecoder(r.Body).Decode(&cita)
	if err != nil {
		utils.SendErr(w, http.StatusBadRequest)
		return
	}
	date, _ := time.Parse("2006-01-02", cita.FechaString)
	cita.Fecha = &date
	err = db.Create(&cita).Error
	if err != nil {
		utils.SendErr(w, http.StatusInternalServerError)
		return
	}
	j, _ := json.Marshal(cita)
	utils.SendResponse(w, http.StatusOK, j)
}

// UpdateCita modifica los dato de una cita
func UpdateCita(w http.ResponseWriter, r *http.Request) {
	citaFind := models.Cita{}
	citaData := models.Cita{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()

	db.Find(&citaFind, id)
	if citaFind.ID > 0 {
		err := json.NewDecoder(r.Body).Decode(&citaData)
		if err != nil {
			utils.SendErr(w, http.StatusBadRequest)
			return
		}
		fecha, _ := time.Parse("2006-01-02", citaData.FechaString)
		citaData.Fecha = &fecha
		err = db.Model(&citaFind).Update(citaData).Error
		if err != nil {
			utils.SendErr(w, http.StatusInternalServerError)
			return
		}
		j, _ := json.Marshal(citaFind)
		utils.SendResponse(w, http.StatusOK, j)
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}

// DeleteCita borra una cita por su ID
func DeleteCita(w http.ResponseWriter, r *http.Request) {
	cita := models.Cita{}
	id := mux.Vars(r)["id"]
	db := utils.GetConnection()
	defer db.Close()

	db.Find(&cita, id)
	if cita.ID > 0 {
		db.Delete(cita)
		utils.SendResponse(w, http.StatusOK, []byte(`{}`))
	} else {
		utils.SendErr(w, http.StatusNotFound)
	}
}
