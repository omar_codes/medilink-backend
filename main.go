package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	"github.com/urfave/negroni"
	"gitlab.com/omar_codes/medilink/routers"
	"gitlab.com/omar_codes/medilink/utils"
)

func main() {
	// Cargar las variables env
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	// Migrar la base datos
	utils.Migrate()
	// Rutas de la app
	router := mux.NewRouter()
	// Agrega las rutas de los endpoints
	routers.SetPacientesRouter(router)
	routers.SetConsultasRouter(router)
	routers.SetMedicinasRouter(router)
	routers.SetCitasRouter(router)
	// Archivos estaticos
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	router.PathPrefix("/").HandlerFunc(indexHandler("apps/index.html"))

	// http.Handle("/", router)

	// CORS
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"},
	})

	// Negroni Middlewares
	n := negroni.Classic()
	n.Use(c)
	n.UseHandler(router)

	port := fmt.Sprintf(":%s", os.Getenv("PORT"))
	server := &http.Server{
		Addr: port,
		// Handler: handlers.CORS()(router),
		Handler: n,
	}

	utils.CleanPDFs().Start()

	log.Printf("Iniciado en http://localhost%s", port)
	log.Println(server.ListenAndServe())
	defer log.Println("Finalizó la ejecución de la aplicación")
}

func indexHandler(entrypoint string) func(w http.ResponseWriter, r *http.Request) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, entrypoint)
	}
	return http.HandlerFunc(fn)
}
