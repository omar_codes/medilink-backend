package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/controllers"
)

// SetConsultasRouter agrega las rutas de consultas
func SetConsultasRouter(r *mux.Router) {
	subRouter := r.PathPrefix("/api").Subrouter()
	subRouter.HandleFunc("/consultas/{id}", controllers.GetConsulta).Methods("GET")
	subRouter.HandleFunc("/consultas", controllers.StoreConsulta).Methods("POST")
	subRouter.HandleFunc("/consultas/{id}", controllers.UpdateConsulta).Methods("PUT")
	subRouter.HandleFunc("/consultas/{id}", controllers.DeleteConsulta).Methods("DELETE")
	subRouter.HandleFunc("/consultas/tratamientos/{id}", controllers.DeleteTratamiento).Methods("DELETE")
}
