package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/controllers"
)

// SetPacientesRouter agrega las rutas de pacientes
func SetPacientesRouter(r *mux.Router) {
	subRouter := r.PathPrefix("/api").Subrouter()
	subRouter.HandleFunc("/pacientes", controllers.GetPacientes).Methods("GET")
	subRouter.HandleFunc("/pacientes/buscar", controllers.SearchPaciente).Methods("GET")
	subRouter.HandleFunc("/pacientes/{id}", controllers.GetPaciente).Methods("GET")
	subRouter.HandleFunc("/pacientes/{id}/consultas", controllers.GetConsultasByPaciente).Methods("GET")
	subRouter.HandleFunc("/pacientes", controllers.StorePaciente).Methods("POST")
	subRouter.HandleFunc("/pacientes/{id}", controllers.UpdatePaciente).Methods("PUT")
	subRouter.HandleFunc("/pacientes/{id}", controllers.DeletePaciente).Methods("DELETE")
}
