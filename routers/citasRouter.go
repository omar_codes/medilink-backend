package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/controllers"
)

// SetCitasRouter agrega las rutas de citas
func SetCitasRouter(r *mux.Router) {
	subRouter := r.PathPrefix("/api").Subrouter()
	subRouter.HandleFunc("/citas/fecha", controllers.GetCitasByFecha).Methods("GET")
	subRouter.HandleFunc("/citas/hoy", controllers.GetCitasToday).Methods("GET")
	subRouter.HandleFunc("/citas/entre", controllers.GetCitasBetweenFechas).Methods("GET")
	subRouter.HandleFunc("/citas/{id}", controllers.GetCita).Methods("GET")
	subRouter.HandleFunc("/citas/paciente/{id}", controllers.GetCitasByPacienteID).Methods("GET")
	subRouter.HandleFunc("/citas", controllers.StoreCita).Methods("POST")
	subRouter.HandleFunc("/citas/{id}", controllers.UpdateCita).Methods("PUT")
	subRouter.HandleFunc("/citas/{id}", controllers.DeleteCita).Methods("DELETE")
}
