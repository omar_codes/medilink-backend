package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/omar_codes/medilink/controllers"
)

// SetMedicinasRouter agrega las rutas para medicinas
func SetMedicinasRouter(r *mux.Router) {
	subRouter := r.PathPrefix("/api").Subrouter()
	subRouter.HandleFunc("/medicinas", controllers.GetMedicinas).Methods("GET")
	subRouter.HandleFunc("/medicinas/suggestions", controllers.GetSuggestions).Methods("GET")
	subRouter.HandleFunc("/medicinas", controllers.StoreMedicina).Methods("POST")
	subRouter.HandleFunc("/medicinas/{id}", controllers.UpdateMedicina).Methods("PUT")
	subRouter.HandleFunc("/medicinas/{id}", controllers.DeleteMedicina).Methods("DELETE")
}
