package models

import "github.com/jinzhu/gorm"

// User modelo para usuarios
type User struct {
	gorm.Model
	Username string `json:"username"`
	Password string `json:"password,omitempty"`
	Reseted  bool   `json:"reseted,omitempty"`
	Role     string `json:"role"`
}

// Roles roles de usuario
var Roles = []string{"Admin", "Doctor", "Dependiente"}
