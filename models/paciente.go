package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Paciente modelo para pacientes
type Paciente struct {
	gorm.Model
	Nombre      string     `json:"nombre" gorm:"not null"`
	FechaString string     `json:"date,omitempty" gorm:"-"`
	Nacimiento  *time.Time `json:"nacimiento"`
	Edad        int        `json:"edad,omitempty" gorm:"-"`
	Telefono    string     `json:"telefono"`
	Celular     string     `json:"celular"`
	Email       string     `json:"email"`
	Direccion   string     `json:"direccion"`
	Sexo        string     `json:"sexo"`
	Descripcion string     `json:"descripcion" gorm:"type:text"`
	Consultas   []Consulta `json:"consultas"`
}
