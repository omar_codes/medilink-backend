package models

import "github.com/dgrijalva/jwt-go"

// Claims for JWT
type Claims struct {
	ID       uint   `json:"ID"`
	Username string `json:"username"`
	jwt.StandardClaims
}
