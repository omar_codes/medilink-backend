package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Cita modelo para citas
type Cita struct {
	gorm.Model
	Estado      string     `json:"estado" gorm:"default:'pendiente'"`
	Fecha       *time.Time `json:"fecha"`
	FechaString string     `json:"date,omitempty" gorm:"-"`
	Hora        string     `json:"hora"`
	PacienteID  uint       `json:"pacienteID"`
}
