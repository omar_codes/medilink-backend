package models

import "github.com/jinzhu/gorm"

// Tratamiento modelo para guardar los datos de la medicacion
type Tratamiento struct {
	gorm.Model
	Medicina     string  `json:"medicina" schema:"medicina"`
	Cantidad     float32 `json:"cantidad" schema:"cantidad"`
	Presentacion string  `json:"presentacion" shema:"presentacion"`
	Dosis        string  `json:"dosis" schema:"dosis"`
	ConsultaID   uint
}
