package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Consulta modelo para consultas
type Consulta struct {
	gorm.Model
	Fecha        *time.Time    `json:"fecha" gorm:"not null"`
	FechaString  string        `json:"date,omitempty" gorm:"-"`
	Diagnostico  string        `json:"diagnostico" gorm:"type:text"`
	Tratamientos []Tratamiento `json:"tratamientos"`
	PacienteID   uint          `json:"pacienteID"`
}
