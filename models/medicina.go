package models

import "github.com/jinzhu/gorm"

// Medicina modelo para guardar los nombres de las medicinas
type Medicina struct {
	gorm.Model
	Nombre string `schema:"nombre"`
}
