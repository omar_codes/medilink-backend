package utils

import (
	"log"

	"github.com/jinzhu/gorm"

	//sqlite
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// GetConnection obtiene la conexion con la base de datos
func GetConnection() *gorm.DB {
	db, err := gorm.Open("sqlite3", "./data/gorm.db")
	if err != nil {
		log.Fatal(err)
	}

	return db
}
