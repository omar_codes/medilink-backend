package utils

import (
	"errors"
	"log"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"gitlab.com/omar_codes/medilink/models"
	"golang.org/x/crypto/bcrypt"
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

// HashPassword encripta
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)
	return string(bytes), err
}

// CheckPassword compara
func CheckPassword(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// GenerateJWT genera JWT
func GenerateJWT(user models.User) (string, error) {
	jwtKey := []byte(os.Getenv("SECRET"))
	expirationTime := time.Now().Add(1 * time.Hour)
	claims := models.Claims{
		ID:       user.ID,
		Username: user.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// DecodeJWT decodifica JWT
func DecodeJWT(tokenStr string) (models.User, error) {
	jwtKey := []byte(os.Getenv("SECRET"))
	claims := models.Claims{}
	tkn, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	user := models.User{}
	if err != nil {
		return user, err
	}
	if !tkn.Valid {
		return user, errors.New("invalid token")
	}
	user.ID = claims.ID
	user.Username = claims.Username
	return user, nil
}
