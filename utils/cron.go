package utils

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/jasonlvhit/gocron"
)

func deletePDFs() {
	fmt.Println("Cleaning PDFs")
	matches, _ := filepath.Glob("static/pdfs/*.pdf")
	for _, file := range matches {
		_ = os.Remove(file)
	}
}

// CleanPDFs elimina los archivos pdf generados
func CleanPDFs() *gocron.Scheduler {
	delScheduler := gocron.NewScheduler()
	delScheduler.Every(1).Hour().Do(deletePDFs)
	return delScheduler
}
