package utils

import (
	"context"
	"net/http"
)

// Auth autentica JWT
func Auth(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	type key string
	jwtToken := r.Header.Get("Authorization")
	user, err := DecodeJWT(jwtToken)
	if err != nil {
		SendErr(w, http.StatusUnauthorized)
		return
	}
	k := key("user")
	ctx := context.WithValue(context.Background(), k, user)
	r.WithContext(ctx)
	next(w, r)
}
