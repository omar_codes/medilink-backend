package utils

import (
	"log"

	"gitlab.com/omar_codes/medilink/models"
)

// Migrate migra los modelos a la base de datos
func Migrate() {
	log.Print("Migrating models...")
	db := GetConnection()
	defer db.Close()
	db.AutoMigrate(&models.Paciente{})
	db.AutoMigrate(&models.Consulta{})
	db.AutoMigrate(&models.Tratamiento{})
	db.AutoMigrate(&models.Medicina{})
	db.AutoMigrate(&models.Cita{})
}
